STDOUT.sync = true # DO NOT REMOVE
# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.


# LX: the X position of the light of power
# LY: the Y position of the light of power
# TX: Thor's starting X position
# TY: Thor's starting Y position
$LX, $LY, $TX, $TY = gets.split(" ").collect {|x| x.to_i}


# game loop
loop do
    $move = ""
    $E = gets.to_i # The level of Thor's remaining energy, representing the number of moves he can still make.
    if $TY > $LY
        STDERR.puts "#{$TY} > #{$LY}"
        $move += "N"
        $TY -= 1
    elsif $TY < $LY
        STDERR.puts "#{$TY} < #{$LY}"
        $move += "S"
        $TY += 1
    elsif $TY == $LY
        STDERR.puts "#{$TY} == #{$LY}"
        $move == ""
    end
    
    if $TX < $LX
        $move += "E"
        $TX += 1
    elsif $TX > $LX
        $move += "W"
        $TX -= 1
    end
    # Write an action using puts
    # To debug: STDERR.puts "Debug messages..."
    
    puts $move # A single line providing the move to be made: N NE E SE S SW W or NW
end